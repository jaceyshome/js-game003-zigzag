myGame.screens["menuScreen"] = (function() {
  //-----------Private variables-------------
  var self = {},
    structure,
    logo = {},
    firstRun = true,
    buttons = {},
    highestScore = {},
    screenVO,
    view = new PIXI.DisplayObjectContainer(),
    LOGO_MAXWIDTH = 700;

  //------------Private functions-------------
  function initLog(structure){
    var charTexts = structure.text.split(''),
      i, charText, color, fontStyle, _font;
    logo = {};
    logo.view = new PIXI.DisplayObjectContainer();
    logo.view.position.x = structure.position.x;
    logo.view.position.y = structure.position.y;
    logo.view.scale.x = structure.scale.x;
    logo.view.scale.y = structure.scale.y;
    logo.width = 0;
    _font =  structure.fontSize.toString() + " " + structure.fontFamily;
    //create each char of logo text
    for (i = 0; i < charTexts.length; i += 1){
      fontStyle = {
        font : _font,
        fill: structure.chars.colors[i]
      };
      charText = new PIXI.Text(
        charTexts[i],
        fontStyle
      );
      charText.position = new PIXI.Point(structure.chars.xPositions[i], structure.chars.yPositions[i]);
      logo.width += charText.width;
      logo.view.addChild(charText);
    }
  }

  function initHighestScore(){
    highestScore = new Score(screenVO.score);
    setHighestScore();
    view.addChild(highestScore.view);
    resetHighestScorePosition();
  }

  function setHighestScore(){
    highestScore.setText(myGame.highestScore);
  }

  function resetHighestScorePosition(){
    highestScore.view.position.y = screenVO.score.y;
    highestScore.view.position.x = myGame.utilities.centralizeObjInScreen(highestScore.view.width, -52);
  }

  function initView(){
    view.position.x = 0;
    view.position.y = 0;
  }

  function initBtnSinglePlayer(){
    buttons["btnSinglePlayer"] = new GraphicButton(screenVO.btnSinglePlayer);
    screenVO.btnSinglePlayer.click = { ftn : onClickBtnSinglePlayer };
    screenVO.btnSinglePlayer.mouseDown = { ftn : onClickBtnSinglePlayer };
  }

  function initBtnReset(){
    screenVO.btnReset.click = {ftn: onClickBtnReset };
    screenVO.btnReset.mouseDown = {ftn: onClickBtnReset };
    buttons["btnReset"] = new GraphicButton(screenVO.btnReset);
  }

  function init() {
    screenVO = myGame.structure.screens.menuScreen;
    initView();
    initLog(screenVO.logo);
    view.addChild(logo.view);
    initBtnSinglePlayer();
    view.addChild(buttons["btnSinglePlayer"].view);
    initBtnReset();
    view.addChild(buttons["btnReset"].view);
    initHighestScore();
    self.resize();
  }

  function onClickBtnSinglePlayer(){
    myGame.game.showScreen("gameScreen");
  }

  function onClickBtnReset(){
    myGame.scoreManager.resetHighestScore();
    setHighestScore();
  }

  function centralizeLogo(width){
    var offset = -40;
    logo.view.position.x = myGame.utilities.centralizeObjInScreen(width,offset);
  }

  function scaleLogo(){
    if(myGame.screenWidth <= LOGO_MAXWIDTH){
      logo.view.scale.x = logo.view.scale.y =  myGame.screenWidth / LOGO_MAXWIDTH;
      centralizeLogo(myGame.screenWidth);
    }else{
      logo.view.scale.x = logo.view.scale.y = 1;
      centralizeLogo(LOGO_MAXWIDTH);
    }
  }

  function centralizeBtn(btnId){
    buttons[btnId].view.position.x = myGame.utilities.centralizeObjInScreen(buttons[btnId].view.width);
  }

  //------------Public functions-----------
  self.hide = function(){
    view.visible = false;
    myGame.stage.removeChild(view);
    return self;
  };

  self.show = function() {
    if (firstRun) {
      init();
      firstRun = false;
    }
    self.resize();
    myGame.stage.addChild(view);
    view.visible = true;
    setHighestScore();
    return self;
  };

  self.resize = function(){
    scaleLogo();
    centralizeBtn("btnSinglePlayer");
    centralizeBtn("btnReset");
    resetHighestScorePosition();
  };

  return self;

})();
