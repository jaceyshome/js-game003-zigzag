myGame.screens["gameScreen"] = (function () {
  //-----------Private variables-------------
  var self = this,
      hero,
      firstRun = true,
      enemies = [],
      clouds = [],
      bulletsManager = undefined,
      collisionManager = undefined,
      enemiesHolder = new PIXI.DisplayObjectContainer(),
      cloudsHolder = new PIXI.DisplayObjectContainer(),
      gameScreenVO = undefined,
      pauseLabel = {},
      score = undefined;
  self.explosionManager = undefined;

  //------------Private functions-------------
  function init() {
    gameScreenVO = myGame.structure.screens.gameScreen;
    collisionManager = new CollisionManager(gameScreenVO);
    initView();
    initClouds();
    initBulletsManager();
    initExplosionManager();
    initEnemies();
    initHero();
    initScore();
    initPauseLabel();
  }

  function initClouds(){
    cloudsHolder.position.x = 0;
    cloudsHolder.position.y = gameScreenVO.cloudsHolderOffsetY;
    var i, j, cloud, spriteTotal = 0;
    for (i = 0; i < gameScreenVO.cloudSpriteNumbers.length; i++){
      spriteTotal = gameScreenVO.cloudSpriteNumbers[i];
      for (j = 0; j < spriteTotal; j++ ){
        cloud = new Cloud(gameScreenVO.cloudSprites[i]);
        clouds.push(cloud);
        cloudsHolder.addChild(cloud.view);
      }
    }
    self.view.addChild(cloudsHolder);
  }

  function initPauseLabel(){
    pauseLabel.view = new PIXI.Text('PAUSED', gameScreenVO.pauseLabel);
    pauseLabel.view.anchor.x = 0.5;
    pauseLabel.view.anchor.y = 0.5;
    self.view.addChild(pauseLabel.view);
    centralizedPauseLabel();
  }

  function centralizedPauseLabel(){
    pauseLabel.view.position.x = myGame.utilities.centralizeObjInScreen(pauseLabel.view.width, -120);
    pauseLabel.view.position.y = myGame.screenHeight / 2;
  }

  function initView() {
    self.view = new PIXI.DisplayObjectContainer();
    self.view.position.x = 0;
    self.view.position.y = 0;
  }

  function initBulletsManager(){
    bulletsManager = new BulletsManager();
    self.view.addChild(bulletsManager.view);
  }

  function initExplosionManager(){
    self.explosionManager = new ExplosionManager();
  }

  function initHero(){
    hero = new Hero(myGame.structure.hero);
    self.view.addChild(hero.view);
  }

  function initEnemies(){
    enemiesHolder.position.x = 0;
    enemiesHolder.position.y = gameScreenVO.enemyContainerOffsetY;
    var i, j, enemy, spriteTotal = 0;
    for (i = 0; i < gameScreenVO.enemySpriteNumbers.length; i++){
      spriteTotal = gameScreenVO.enemySpriteNumbers[i];
      for (j = 0; j < spriteTotal; j++ ){
        enemy = new Enemy(gameScreenVO.enemySprites[i]);
        enemies.push(enemy);
        enemiesHolder.addChild(enemy.view);
      }
    }
    self.view.addChild(enemiesHolder);
  }

  function initScore(){
    score = new Score(gameScreenVO.score);
    self.view.addChild(score.view);
  }

  function updateEnemies(){
    var i;
    for (i=0; i < enemies.length; i++){
      enemies[i].update();
      collisionManager.enemyWidthBullets(enemies[i], bulletsManager.getBullets());
    }
  }

  function updateClouds(){
    var i;
    for (i=0; i < clouds.length; i++){
      clouds[i].update();
    }
  }

  function handleHeroFireRate(){
    if(hero.life <= 0){
      return;
    }
    var newBulletData;
    if(hero.fireDelay){
      hero.fireDelay -= 1;
    }else{
      newBulletData = {
        belong: "hero",
        position: {x: hero.view.position.x, y: hero.view.position.y - 15},
        speed:myGame.structure.bulletTypes[hero.bulletType].speed,
        sprite:myGame.structure.bulletTypes[hero.bulletType].sprite,
        power:myGame.structure.bulletTypes[hero.bulletType].power,
        sound:myGame.structure.bulletTypes[hero.bulletType].sound
      };
      bulletsManager.fireBullet(newBulletData);
      hero.resetFireDelay();
    }
  }

  function updateMap(){
//    var posY = myGame.googleMap.getCenter().k + 0.0001;
//    var posA = myGame.googleMap.getCenter().A;
//    myGame.googleMap.panTo(new google.maps.LatLng(posY, posA));
  }

  function recordHeroScore(){
    if(hero.score > myGame.highestScore){
      myGame.highestScore = hero.score;
      myGame.scoreManager.setHighestScore();
    }
  }

  function resetEnemies(){
    var i = 0;
    for(i; i < enemies.length; i++){
      enemies[i].reset();
    }
  }

  function resetClouds(){
    var i = 0;
    for(i; i < clouds.length; i++){
      clouds[i].reset();
    }
  }

  function showPauseLabel(){
    pauseLabel.view.visible = true;
  }

  function hidePauseLabel(){
    pauseLabel.view.visible = false;
  }

  function pauseScreen(){
    if(!pauseLabel.view.visible){
      showPauseLabel();
    }
  }

  function unpauseScreen(){
    if(pauseLabel.view.visible){
      hidePauseLabel();
    }
  }

  function startHero(){
    hero.view.position.x = myGame.screenWidth / 2;
    hero.view.position.y = myGame.screenHeight / 3 * 2;
  }

  //------------Public functions-----------
  self.update = function(){
    if(firstRun){
      return;
    }
    if(myGame.pausedActivatedScreen){
      pauseScreen();
      return;
    }else{
      unpauseScreen();
    }
    bulletsManager.update();
    //hero.update(); //Jake: hero register event listener for updating position
    collisionManager.heroWidthEnemy(hero,enemies);
    handleHeroFireRate();
    updateEnemies();
    updateClouds();
    updateMap();
    myGame.levelManager.update();
  };

  self.hide =function() {
    reset();
    self.view.visible = false;
    myGame.stage.removeChild(self.view);
    return self;
  };

  self.show = function() {
    if (firstRun) {
      init();
      firstRun = false;
    }else{
      hero.restart();
    }
    startHero();
    myGame.levelManager.reset();
    myGame.stage.addChild(self.view);
    self.view.visible = true;
    return self;
  };

  self.resize = function(){
    centralizedPauseLabel();
    score.setPosition();
  };

  self.updateHeroScore = function(val){
    hero.score += val;
    score.setText(hero.score.toString());
    recordHeroScore();
  };

  self.reset = function(){
    resetEnemies();
    score.setText(0);
    myGame.levelManager.reset();
    self.explosionManager.reset();
    hero.reset();
    resetClouds();
//    saveScore();
  };

  self.getHeroScore = function(){
    return hero.score;
  };

  return self;

})();
