// Global variables
Function.prototype.bind = function (bind) {
  var self = this;
  return function () {
    var args = Array.prototype.slice.call(arguments);
    return self.apply(bind || null, args);
  };
};

var myGame = (function (){
  var entityTotal = 0;
  function generateUID(){
    var id = "entity" + entityTotal;
    entityTotal += 1;
    return id;
  }

  return {
    name : "ZIG ZAG",
    screenWidth : 800,
    screenHeight : 600,
    bgColor: 0x000000,
    renderer: undefined,
    stage: undefined,
    activeScreen: undefined,
    structure : undefined,
    bitMapFonts : [],
    screens: {},
    entities: [],
    generateUID : generateUID,
    move_dir: {x:0, y:0},
    timer: {previousTime:0, time:0},
    images: undefined,
    highestScore: 0
  }
})();

//Game assets Loader

myGame.loader = (function(){
  var _callback;

  function load(callback){
    _callback = callback;
    loadStructure({
      url: "assets/structure.json",
      callback: loadAssets
    });
  }

  function loadAssets(data){
    var assets = myGame.structure.assets,
        assetsUrls = [],
        loader;
    for (var key in assets) {
      var obj = assets[key];
//      TODO need to check?
//      for (var prop in obj) {
//        // important check that this is objects own property
//        // not from prototype prop inherited
//        if(obj.hasOwnProperty(prop)){
//          alert(prop + " = " + obj[prop]);
//        }
//      }
      assetsUrls.push(obj);
    }
    loader = new PIXI.AssetLoader(assetsUrls);
    loader.onProgress = function(){
//      console.log("loading", loader);
    };
    loader.onComplete = function(evt){
//      console.log("finish loading assets", PIXI);
      if(_callback){
        _callback();
      }
    };
    loader.load();
  }

  function loadStructure(data){
    var loader = new PIXI.JsonLoader(data.url);
    loader.on('loaded', function(evt) {
      myGame.structure = evt.content.json;
      if(data.callback){
        data.callback();
      }
    });
    loader.load();
  }

  return {
    load : load
  }
})();

//Game script loader
window.addEventListener("load", function(){
  Modernizr.load([
    {
      load : [
        "vendor/sizzle.js",
        "vendor/pixi.dev.js",

        "src/utilities.js",
        "src/entity/entity.js",
        "src/entity/components/action.js",
        "src/entity/components/create-button-view-object.js",
        "src/entity/components/mover.js",
        "src/entity/components/transform.js",

        "src/components/hero.js",
        "src/components/enemy.js",
        "src/components/bg-image.js",
        "src/components/bg-tiled-map.js",
        "src/components/graphic-button.js",
        "src/components/texture-button.js",
        "src/components/bullet.js",
        "src/components/score.js",
        "src/components/explosion.js",
        "src/components/cloud.js",

        "src/managers/request_animation_frame.js",
        "src/managers/localstorage-manager.js",
        "src/managers/score-manager.js",
        "src/managers/sound-manager.js",
        "src/managers/input-manager.js",
        "src/managers/sound-manager.js",
        "src/managers/explosion-manager.js",
        "src/managers/bullets-manager.js",
        "src/managers/collision-manager.js",
        "src/managers/level-manager.js",
        "src/managers/game-engine.js",

        "src/screens/menu.js",
        "src/screens/game.js",

        "src/game.js"
      ],
      complete : function(){
        myGame.soundManager.init();
        var callback = function(){
          myGame.game.init().showScreen("menuScreen");
        };
        myGame.loader.load(callback);
      }
    }
  ]);
});

