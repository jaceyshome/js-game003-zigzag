var Explosion = function(customData){
  var self = this,
      data = customData || {},
      firstRun = true,
      textures = [];

  self.view = undefined;

  function init(){
    if(!firstRun){
      return;
    }
    initMovieClip();
    initParameters();
    myGame.activeScreen.view.addChild(self.view);
  }

  function initParameters(){
    self.life = data.life || 1;
    self.delay = data.delay || 0;
    self.view.position.x = 0;
    self.view.position.y = 0;
    self.view.anchor.x = 0.5;
    self.view.anchor.y = 0.5;
    self.view.rotation = Math.random() * Math.PI;
    self.view.scale.x = self.view.scale.y = 1;
    self.view.loop = false;
    return self;
  }


  function initTextures(){
    var i = 0, name;
    for (i=0;i<data.textureTotal;i++){
      name = data.textureName + (myGame.utilities.pad((i+1),3) + ".png");
      textures.push(PIXI.Texture.fromFrame(name));
    }
    return self;
  }

  function initMovieClip(){
    initTextures();
    self.view = new PIXI.MovieClip(textures);
    return self;
  }

  self.play = function(){
    self.view.gotoAndPlay(0);
  };

  self.setScale = function(val){
    self.view.scale.x = self.view.scale.y = val || 1;
  };


  init();
  return self;
};
//Inheritance Entity Prototype
Explosion.prototype = Object.create(Entity.prototype);

