var Enemy = function(customData){
  var self = {},
      data = customData || {},
      firstRun = true,
      explosion = undefined,
      collisionMasks = {},
      body = {};

  function initParameters(){
    self.score = data.score || 0;
    self.life = data.life || 100;
    self.speed = data.speed || 30;
    self.delay = getDelay(data.delay);
    self.startPosition = {
      x: getPositionX(),
      y: 0
    };
  }

  function getPositionX(){
    return Math.floor((Math.random() * (myGame.screenWidth/10)) + 10) *10;
  }

  function getDelay(){
    return Math.floor((Math.random() * 10) + 1) * 100;
  }

  function init(){
    if(firstRun){
      Entity.call(self, data);
      initParameters();
      initView();
      firstRun = false;
    }
    return self;
  }

  function initView(){
    self.view = new PIXI.DisplayObjectContainer();
    initBody();
    initCollisionMasks();
    self.view.position.x = self.startPosition.x;
    self.view.position.y = self.startPosition.y;
  }

  function initBody(){
    body.view = PIXI.Sprite.fromFrame(data.sprite);
    body.view.anchor.x = 0.5;
    body.view.anchor.y = 0.5;
    body.view.position.x = 0;
    body.view.position.y = 0;
    self.view.addChild(body.view);
  }

  function initCollisionMasks(){
    if(!data.collisionMasks){
      return;
    }
    collisionMasks = {};
    collisionMasks.wing = createWingMask(data.collisionMasks.wing);
    collisionMasks.wing.visible = false;
    collisionMasks.body = createBodyMask(data.collisionMasks.body);
    collisionMasks.body.visible = false;
    self.view.addChild(collisionMasks.wing);
    self.view.addChild(collisionMasks.body);
  }

  function createBodyMask(data){
    var x = - body.view.width / 2 + (body.view.width - data.w) / 2,
        y = - body.view.height / 2,
        w = data.w,
        h = body.view.height,
    mask = new PIXI.Graphics();
    mask.beginFill("0xFFFFFF");
    mask.drawRect(0,0,w,h);
    mask.endFill();
    mask.position.x = x;
    mask.position.y = y;
    mask.width = w;
    mask.height = h;
    mask.alpha = 0.5;
    return mask;
  }

  function createWingMask(data){
    var x = - body.view.width / 2,
        y = data.y || 0,
        w = body.view.width,
        h = data.h,
    mask = new PIXI.Graphics();
    mask.beginFill("0xFF0000");
    mask.drawRect(0,0,w,h);
    mask.endFill();
    mask.alpha = 0.5;
    mask.position.x = x;
    mask.position.y = y;
    mask.width = w;
    mask.height = h;
    return mask;
  }

  function resetPosition(){
    self.view.position.x = getPositionX();
    self.view.position.y = self.startPosition.y;
  }

  function checkState(){
    if(self.view.position.y >= myGame.screenHeight + 400){
      hideBody();
      self.reset();
    }
  }

  self.reset = function(){
    resetPosition();
    self.life = data.life || 100;
    self.delay = getDelay();
  };

  function destroy(){
    if(myGame.activeScreen.explosionManager){
      hideBody();
      playExplosion();
    }else{
      self.reset();
    }
    playExplosionSound();
    sendHeroScore();
  }

  function playExplosionSound(){
    myGame.soundManager.playAudio({
      id:self.uid,
      url:"assets/sounds/explosion.wav"
    })
  }

  function hideBody(){
    body.view.visible = false;
  }

  function showBody(){
    body.view.visible = true;
  }

  function playExplosion(){
    explosion = myGame.activeScreen.explosionManager.getExplosion();
    self.view.addChild(explosion.view);
    explosion.setScale(data.explosionScale);
    explosion.view.visible = true;
    explosion.play();
    explosion.view.onComplete = onExplosionCompleted;
  }

  function onExplosionCompleted(){
    if(explosion){
      explosion.view.visible = false;
      explosion.view.onComplete = undefined;
      explosion = null;
      self.reset();
    }
  }

  function sendHeroScore(){
    if(myGame.activeScreen.updateHeroScore){
      myGame.activeScreen.updateHeroScore(self.score);
    }
  }

  self.getDamage = function(val){
    self.life -= val;
    if(self.life<=0){
      destroy();
    }
  };

  self.update = function(){
    if(firstRun){
      return;
    }
    if( self.life <= 0){
      return;
    }
    if(self.delay > 0){
      self.delay--;
      return;
    }
    if(!body.view.visible){
      showBody();
    }
    self.view.position.y += (myGame.deltaTime * data.speed + myGame.levelManager.getSpeedOffset());
    checkState();
  };

  self.getWidth = function(){
    return body.view.width;
  };

  self.getHeight = function(){
    return body.view.height;
  };

  self.getCollisionMasks = function(){
    return collisionMasks;
  };

  init();
  return self;
};
//Inheritance Entity Prototype
Enemy.prototype = Object.create(Entity.prototype);

