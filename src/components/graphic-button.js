var GraphicButton = function(_data){
	Entity.call(this, _data);
	this.addComponent("createButtonViewObject");

	//Private variables
	var self = this,
		data = _data || {},
		text, bg,
		width = data.width,
		height = data.height;

	//Public variables
	this.view = self._components.createButtonViewObject(new PIXI.DisplayObjectContainer(), data);

	//Private functions
	function createText(){
		text = new PIXI.Text(data.mouseOut.text.text, data.mouseOut.fontStyle);
		if(data.mouseOut.text.position){
			text.position = new PIXI.Point(data.mouseOut.text.position.x, data.mouseOut.text.position.y);
		}
		self.view.addChild(text);
	}

	function drawBgRect(bgData){
		bg.beginFill(bgData.color);
		if(bgData.lineStyle){
			bg.lineStyle(bgData.lineStyle);
		}
		bg.drawRect(0,0,width,height);
	}

	function createBg(){
		//button bg
		bg = new PIXI.Graphics();
		drawBgRect(data.mouseOut.bg);
		self.view.addChild(bg);
	}

	//Public functions
	function init(){
		createBg();
		createText();
		return self;
	}

	this.destroy = function(){
		parent.removeChild(self.view);
		return this;
	};

	this.show = function(){
		this.view.visible = true;
		return this;
	};

	this.hide = function(){
		this.view.visible = false;
		return this;
	};

	//Button event handlers
	data.onMouseDown = function(){
		if(data.mouseDown.ftn){
			data.mouseDown.ftn();
		}
		if(data.mouseDown.fontStyle){
			text.setStyle(data.mouseDown.bg.fontStyle);
		}
		if(data.mouseDown.bg){
			drawBgRect(data.mouseDown.bg);
		}
	};
	data.onMouseUp = function(){
		if(!data.mouseUp){
			return;
		}
		if(data.mouseUp.ftn){
			data.mouseUp.ftn();
		}
	};
	data.onMouseOver = function(){
		if(!data.mouseOver){
			return;
		}
		if(data.mouseOver.ftn){
			data.mouseOver.ftn();
		}
		if(data.mouseOver.fontStyle){
			text.setStyle(data.mouseOver.fontStyle);
		}
		if(data.mouseOver.bg){
			drawBgRect(data.mouseOver.bg);
		}
	};
	data.onMouseOut = function(){
		if(!data.mouseOut){
			return;
		}
		if(data.mouseOut.ftn){
			data.mouseOut.ftn();
		}
		if(data.mouseOut.fontStyle){
			text.setStyle(data.mouseOut.fontStyle);
		}
		if(data.mouseOut.bg){
			drawBgRect(data.mouseOut.bg);
		}
	};
	data.onClick = function(){
		if(!data.click){
			return;
		}
		if(data.click.ftn){
			data.click.ftn();
		}
	};
	data.onTap = function(){
		if(!data.tap){
			return;
		}
		if(data.tap.ftn){
			data.tap.ftn();
		}
	};

	init();
	return this;
};
//Inheritance Entity Prototype
GraphicButton.prototype = Object.create(Entity.prototype);
