var Score = function(customData){
  var self = this,
    data = customData || {},
    totalDigit = 8,
    digitWidth = 8,
    firstRun = true;

  function initParameters(){
    self.score = data.score || 0;
  }

  function initView(){
    self.view = new PIXI.Text('00000000', data.fontStyle);
    self.view.anchor.x = 0.5;
    self.view.anchor.y = 0.5;
    self.setPosition();
  }

  function init(){
    if(firstRun){
      Entity.call(self, data);
      initParameters();
      initView();
      firstRun = false;
    }
    return self;
  }

  self.reset = function(){
    return self;
  };

  self.setPosition = function(){
    self.view.position.x = myGame.screenWidth - totalDigit * digitWidth;
    self.view.position.y = 20;
    return self;
  };

  self.setText = function(val){
    self.view.setText(myGame.utilities.pad(val || 0,totalDigit));
  };

  init();
  return self;
};
//Inheritance Entity Prototype
Score.prototype = Object.create(Entity.prototype);
