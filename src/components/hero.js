var Hero = function(customData){
  var self = this,
      data = customData || {},
      explosion = undefined,
      body = {},
      firstRun = true;
  self.dragging = false;

  function initParameters(){
    self.score = data.score || 0;
    self.life = data.life || 100;
    self.experience =  data.experience || 0;
    self.speed = data.speed || 30;
    self.bulletType = data.bulletType;
    self.startPosition = {
      x: 0,
      y: 0
    };
    self.previousMove_dir = {x: 0, y: 0};
    self.resetFireDelay();
  }

  function initView(){
    self.view = new PIXI.DisplayObjectContainer();
    initBody();
    self.view.position.x = self.startPosition.x;
    self.view.position.y = self.startPosition.y;
    self.view.width = body.view.width;
    self.view.height = body.view.height;
  }

  function initBody(){
    body.view = PIXI.Sprite.fromFrame(data.sprite);
    body.view.anchor.x = 0.5;
    body.view.anchor.y = 0.5;
    body.view.position.x = 0;
    body.view.position.y = 0;
    self.view.addChild(body.view);
  }

  function init(){
    if(firstRun){
      Entity.call(self, data);
      initParameters();
      initView();
      initTouchEvents();
      firstRun = false;
    }
    return self;
  }

  function initTouchEvents(){
    body.view.interactive = true;
    body.view.touchstart = function(){
      self.dragging = true;
    };
    body.view.touchmove = function(data){
      if(!body.view.visible){
        return;
      }
      if(self.dragging){
        self.view.position.x = data.global.x;
        self.view.position.y = data.global.y;
      }
    };
    body.view.touchend = function(){
      self.dragging = false;
    };
    body.view.mousemove = function(data){
      if(!body.view.visible){
        return;
      }
      self.view.position.x = data.global.x;
      self.view.position.y = data.global.y;
    }
  }

  function playExplosion(){
    explosion = myGame.activeScreen.explosionManager.getExplosion();
    self.view.addChild(explosion.view);
    explosion.setScale(0.8);
    explosion.view.visible = true;
    explosion.play();
    explosion.view.onComplete = onExplosionCompleted;
  }

  function onExplosionCompleted(){
    if(explosion){
      explosion.view.visible = false;
      explosion.view.onComplete = undefined;
      explosion = null;
    }
    self.view.visible = false;
  }

  function hideBody(){
    body.view.visible = false;
  }

  function showBody(){
    body.view.visible = true;
  }

  self.destroy = function(){
    self.life = 0;
    hideBody();
    myGame.game.setGameOver();
    playExplosion();
  };

  self.restart = function(){
    showBody();
    self.view.visible = true;
    self.life = data.life || 100;
    return self;
  };

  self.save = function(){
    return self;
  };

  self.resetFireDelay = function(){
    self.fireDelay = data.defaultFireDelay;
  };

  self.reset = function(){
    self.score = 0;
  };

  init();
  return self;
};
//Inheritance Entity Prototype
Hero.prototype = Object.create(Entity.prototype);
