var Cloud = function(customData){
  var self = {},
    data = customData || {},
    firstRun = true;

  function initParameters(){
    self.score = data.score || 0;
    self.speed = data.speed || 30;
    self.startPosition = {
      x: getPositionX(),
      y: 0
    };
  }

  function getPositionX(){
    return Math.floor((Math.random() * (myGame.screenWidth/10)) + 10) *11;
  }

  function init(){
    if(firstRun){
      Entity.call(self, data);
      initParameters();
      initView();
      firstRun = false;
    }
    return self;
  }

  function initView(){
    self.view = PIXI.Sprite.fromFrame(data.sprite);
    self.view.anchor.x = 0.5;
    self.view.anchor.y = 0.5;
    resetPosition();
    resetScale();
    resetRotation();
    resetSpeed();
    resetDelay();
  }

  function resetPosition(){
    self.view.position.x = getPositionX();
    self.view.position.y = self.startPosition.y;
  }

  function resetScale(){
    self.view.scale.x = self.view.scale.y = Math.floor(Math.random() * 11 + 5) * 0.1;
  }

  function resetRotation(){
    self.view.rotation = Math.random() * Math.PI;
  }

  function resetSpeed(){
    self.speed = Math.floor(Math.random() * 5 + 2) * 0.1;
  }

  function resetDelay(){
    self.delay =  Math.floor((Math.random() * 10) + 1) * 100;
  }

  function checkState(){
    if(self.view.position.y >= myGame.screenHeight + 500){
      self.reset();
    }
  }

  self.reset = function(){
    resetPosition();
    resetScale();
    resetRotation();
    resetSpeed();
    resetDelay();
  };

  self.update = function(){
    if(firstRun){
      return;
    }
    if(self.delay > 0){
      self.delay--;
      return;
    }
    self.view.position.y += (myGame.deltaTime * self.speed);
    checkState();
  };

  init();
  return self;
};
//Inheritance Entity Prototype
Cloud.prototype = Object.create(Entity.prototype);

