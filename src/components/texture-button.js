var TextureButton = function(_data){
	Entity.call(this, _data);
	this.addComponent("createButtonViewObject");

	//Private variables
	var data = _data || {};
	var self = this;

	var textures = {};
	textures["mouseOut"] = PIXI.Texture.fromImage("assets/images/button.png");
	textures["mouseOver"] = PIXI.Texture.fromImage("assets/images/buttonOver.png");
	textures["mouseDown"] = PIXI.Texture.fromImage("assets/images/buttonDown.png");
	textures["mouseDisable"] = PIXI.Texture.fromImage("assets/images/button.png");

	//button state
	if(!data.onMouseDown){
		data.onMouseDown = function(){
			object.setTexture(textures.mouseDown);
			object.alpha = 1;
		}
	}
	if(!data.onMouseUp){
		data.onMouseUp = function(){
			object.setTexture(textures.mouseOver);
		}
	}
	if(!data.onMouseOver){
		data.onMouseOver = function(){
			object.setTexture(textures.mouseOver);
		}
	}
	if(!data.onMouseOut){
		data.mouseOut = function(){
			object.setTexture(textures.mouseOut);
		}
	}

	var object = this._components.createButtonViewObject(new PIXI.Sprite(textures.mouseOut), data);
	object.anchor.x = object.anchor.y = 0.5;



	//Public variables
	this.position = object.position;
	this.rotation = object.rotation;
	this.scale = object.scale;

	//Public functions
	this.init = function(){
		myGame.stage.addChild(object);
		return this;
	};

	this.destroy = function(){
		myGame.stage.removeChild(object);
		return this;
	};

	this.show = function(){
		object.visible = true;
		return this;
	};

	this.hide = function(){
		object.visible = false;
		return this;
	};

	return this;
};
//Inheritance Entity Prototype
TextureButton.prototype = Object.create(Entity.prototype);
