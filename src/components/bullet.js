var Bullet = function(customData){
  var self = this,
      data = customData || {},
      firstRun = true;

  function initParameters(){
    self.life = data.life || 1;
    self.speed = data.speed || 10;
    self.delay = data.delay || 0;
    self.sprite = data.sprite;
    self.power = data.power || 10;
    self.position = {
      x: data.position.x,
      y: data.position.y
    };
  }

  function initView(){
    self.view = PIXI.Sprite.fromFrame(data.sprite);
    self.view.anchor.x = 0.5;
    self.view.anchor.y = 0.5;
    resetPosition();
  }

  function checkState(){
    if(self.view.position.y <= -50){
      self.destroy();
    }
  }

  function resetPosition(){
    self.view.position.x = 0;
    self.view.position.y = 0;
  }

  function reset(){
    resetPosition();
    self.view.visible = false;
    self.delay = data.delay || 0;
  }

  function init(){
    if(firstRun){
      Entity.call(self, data);
      initParameters();
      initView();
      firstRun = false;
      self.view.visible = false;
    }
    return self;
  }

  self.update = function(){
    if(firstRun || !self.life){
      return;
    }
    self.view.visible = true;
    self.view.position.y -= myGame.deltaTime * data.speed;
    checkState();
  };

  self.setPosition = function(pos){
    self.view.position.x = pos.x;
    self.view.position.y = pos.y;
  };

  self.destroy = function(){
    self.view.visible = false;
    self.life = 0;
    reset();
  };

  init();
  return self;
};
//Inheritance Entity Prototype
Bullet.prototype = Object.create(Entity.prototype);

