var bgTiledMap = function (customData) {
  var self = this, sprites = [],
      data = customData || {},
      texture = undefined,
      layers = [],
      firstRun = true;

  function initParameters() {
    self.speed = data.speed || 10;
    self.animationEnabled = data.animationEnabled || false;
    self.repeat = data.repeat || {
      x: 0,
      y: 0
    };
    return self;
  }

  function initView() {
    self.view = new PIXI.DisplayObjectContainer();
    self.view.position.x = 0;
    self.view.position.y = 0;
    texture = new PIXI.Texture.fromImage(data.image);
    createImageSprites(texture);
  }

  function createImageSprites(texture){
    var i, total, x = 0, y = 0,
        offsetX = 0, offsetY = 0;
    if(data.repeat.y){
      total = data.repeat.y;
      offsetY = data["height"];
    }
    if(data.repeat.x){
      total = data.repeat.x;
      offsetX = data["width"];
    }
    for (i=0; i < total; i++)
    {
      var bgSprite = new PIXI.Sprite(texture);
      bgSprite.position.x = x;
      bgSprite.position.y = y;
      sprites.push({sprite: bgSprite, x: bgSprite.position.x, y: bgSprite.position.y});
      self.view.addChild(bgSprite);
      y -= offsetY;
      x -= offsetX;
    }
    return sprites;
  }

  function init() {
    if (firstRun) {
      Entity.call(self, data);
      initParameters();
      initView();
      firstRun = false;
    }
    return self;
  }

  this.restart = function () {
    return this;
  };

  this.restore = function () {
    return this;
  };

  this.save = function () {
    return this;
  };

  this.update = function () {
    if (firstRun || !self.animationEnabled) {
      return;
    }
    var deltaTime = myGame.deltaTime;
    self.view.position.y += self.speed * deltaTime;
  };
  init();
  return this;
};
//Inheritance Entity Prototype
Hero.prototype = Object.create(Entity.prototype);
