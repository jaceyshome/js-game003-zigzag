var bgImage = function (customData) {
  var self = this, sprites = [],
      data = customData || {},
      texture = undefined,
      firstRun = true,
      total = 0,
      children = [],
      bgSpriteHeight = 0;

  function initParameters() {
    self.speed = data.speed || 0.002;
    self.animationEnabled = data.animationEnabled || false;
    self.repeat = data.repeat || {
      x: 0,
      y: 0
    };
    return self;
  }

  function initView() {
    self.view = new PIXI.DisplayObjectContainer();
    self.view.position.x = 0;
    self.view.position.y = 0;
    texture = new PIXI.Texture.fromImage(data.image);
    children = createImageSprites(texture);
  }

  function createImageSprites(texture){
    var i, x = 0, y = 0,
        offsetX = 0, offsetY = 0;
    if(data.repeat.y){
      total = data.repeat.y;
      bgSpriteHeight = data["height"];
    }
    if(data.repeat.x){
      total = data.repeat.x;
      offsetX = data["width"];
    }
    for (i=1; i <= total; i++)
    {
      var bgSprite = new PIXI.Sprite(texture);
      bgSprite.position.x = x;
      bgSprite.position.y = y;
      sprites.push(bgSprite);
      self.view.addChild(bgSprite);
      y -= bgSpriteHeight;
      x -= offsetX;
    }
    return sprites;
  }

  function init() {
    if (firstRun) {
      Entity.call(self, data);
      initParameters();
      initView();
      firstRun = false;
    }
    return self;
  }

  function checkAnimatedDistance(sprite){
    if(sprite.position.y >= self.view.position.y + bgSpriteHeight){
      resetSpriteHeight(sprite);
    }
  }

  function resetSpriteHeight(sprite){
    var offset = sprite.position.y - bgSpriteHeight;
    sprite.position.y = -bgSpriteHeight + offset ;
  }

  this.restart = function () {
    return this;
  };

  this.restore = function () {
    return this;
  };

  this.save = function () {
    return this;
  };

  this.update = function () {
    var index = 0, child, deltaTime = myGame.deltaTime;
    if (firstRun || !self.animationEnabled) {
      return;
    }

    for(index = 0; index < children.length; index++ ){
      child = children[index];
      if(child.position.y >= self.view.position.y + bgSpriteHeight){
        resetSpriteHeight(child);
      }
      child.position.y += self.speed * deltaTime;
    }
  };
  init();

  self.resize = function(){
    var index = 0;
    var scaleX = myGame.screenWidth / data["width"];
    for(index = 0; index < children.length; index++ ){
      children[index].scale.x = scaleX;
    }
  };
  return this;
};
//Inheritance Entity Prototype
Hero.prototype = Object.create(Entity.prototype);
