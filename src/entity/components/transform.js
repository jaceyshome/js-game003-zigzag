Component.add("transform", function() {
  var firstRun = true;

  function init(){
    console.log("init my transform component");
  }

  function run(){
    if (firstRun){
      firstRun = false;
      init();
    }
  }

  return {
    run : run,
    position: [0,0],
    rotation: 0,
    scale: 1
  }
});