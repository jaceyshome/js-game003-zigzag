Component.add("createButtonViewObject", function(){
  return function(object, data){
    object.buttonMode = true;
    object.interactive = true;
    if(data.position){
      object.position.x = data.position.x;
      object.position.y = data.position.y;
    }else{
      object.position.x = 0;
      object.position.y = 0;
    }
    object.width = data.width;
    object.height = data.height;
    object.hitArea = new PIXI.Rectangle(0,0,object.width, object.height);

    //Private functions
    object.mousedown = object.touchstart = function(){
      object.isdown = true;
      if(data.onMouseDown){
        data.onMouseDown();
      }
    };

    object.mouseup = object.touchend = object.mouseupoutside = object.touchendoutside = function(){
      object.isdown = false;
      if(data.onMouseUp){
        if(object.isOver){
          if(data.onMouseOver){
            data.onMouseOver();
          }
        }else{
          if(data.onMouseOut){
            data.onMouseOut();
          }
        }
      }
    };

    object.mouseover = function(){
      object.isOver = true;
      if(object.isdown){
        return;
      }
      if(data.onMouseOver){
        data.onMouseOver();
      }
    };

    object.mouseout = function(){
      object.isOver = false;
      if(object.isdown){
        return;
      }
      if(data.onMouseOut){
        data.onMouseOut();
      }
    };

    object.click = function(){
      if(data.onClick){
        data.onClick();
      }
    };

    object.tap = function(){
      if(data.onTap){
        data.onTap();
      }
    };

    return object;
  };
});