Component.add("mover", function() {
  var firstRun = true;

  function init(){
    this._transform = entity.getComponent("transform");
  }

  function run(){
    var deltaTime = 0.2;
    this._transform.translation[0]  = this._transform.translation[0] + 7 * deltaTime;
    this._transform.translation[1]  = this._transform.translation[1] + 7 * deltaTime;
  }

  return {
    init: init,
    run: run
  };
});