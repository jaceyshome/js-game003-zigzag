var Component = function (name) {
	var component = Component._registry[name]();
	component.__uid = myGame.generateUID();
	return component;
};

Component._registry = {};

Component.add = function (name, factory) {
	Component._registry[name] = factory;
};

var Entity = function(_initData) {
	var initData = _initData || {};
	this.uid = myGame.generateUID();
	this.name = initData.name || this.uid;
//	this.position = initData.position || {x:0,y:0};
//	this.rotation = initData.rotation || 0;
//	this.scale = initData.scale || 1;
//	this.visible = initData.visible || true;
	this.zIndex = initData.zIndex || 0;
	this._components = {};
	myGame.entities.push(this);
};

Entity.prototype = {
	addComponent: function (type) {
		return this._components[type] = Component(type);
	},
	getComponent: function (type) {
		return this._components[type];
	},
	removeComponent: function (type) {
		this._components.splice(type,1); //need to double check
	}
};



