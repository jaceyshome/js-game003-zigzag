myGame.game = (function(){
  var firstRun = true;

  function showScreen(screenId){
    var activeScreen = myGame.activeScreen;
    if(activeScreen){
      hideScreen(activeScreen);
      myGame.activeScreen = null;
    }
    myGame.activeScreen = myGame.screens[screenId];
    myGame.activeScreen.show();
    resize();
    return this;
  }

  function hideScreen(screen){
    myGame.activeScreen.hide();
    return this;
  }

  function initGameStage(){
    var interactive = true;
    myGame.stage = new PIXI.Stage(myGame.bgColor, interactive);
//    myGame.renderer = new PIXI.CanvasRenderer(myGame.screenWidth,myGame.screenHeight);
//    myGame.renderer = new PIXI.WebGLRenderer(myGame.screenWidth,myGame.screenHeight);
    myGame.renderer = PIXI.autoDetectRenderer(myGame.screenWidth,myGame.screenHeight, null, true);
    myGame.renderer.view.id = "myGameView";
    document.body.appendChild(myGame.renderer.view);
  }

  function cycle() {
    var passTime;
    myGame.timer.time = Date.now();
    passTime = myGame.timer.time - myGame.timer.previousTime;
    myGame.deltaTime = Math.max(0, Math.min(1, passTime));
    myGame.gameEngine.update();
    myGame.timer.previousTime = myGame.timer.time;
    myGame.renderer.render(myGame.stage);
    requestAnimationFrame(cycle);
  }

  function resize(){
    var newWidth = window.innerWidth;
    var newHeight = window.innerHeight;
    myGame.screenWidth = newWidth;
    myGame.screenHeight = newHeight;
    myGame.renderer.resize(newWidth, newHeight);
    if(myGame.activeScreen){
      if(myGame.activeScreen.resize){
        myGame.activeScreen.resize();
      }
    }
  }

  function initGameHighestScore(){
    myGame.highestScore = myGame.scoreManager.getHighestScore();
  }

  function initLevelManager(){
    myGame.levelManager.init();
  }

  function init(){
    if(firstRun){
      initGameStage();
      initGameHighestScore();
      initLevelManager();
      initWindowResize();
      myGame.gameEngine.init();
      resize();
      cycle();
      firstRun = false;
    }
    return this;
  }

  function setGameOver(){
    setTimeout(function(){myGame.game.showScreen("menuScreen")},3000);
  }

  function initWindowResize(){
    window.onresize = function(event) {
      resize();
    };
  }

  return {
    init:init,
    showScreen: showScreen,
    hideScreen: hideScreen,
    setGameOver: setGameOver
  }

})();