myGame.scoreManager = (function(){
  var self = {},
      key = "highestScore";

  self.getHighestScore = function(){
    return myGame.localStorage.load(key);
  };

  self.setHighestScore = function(){
    return myGame.localStorage.save(key,myGame.highestScore);
  };

  self.resetHighestScore = function(){
    myGame.highestScore = 0;
    return myGame.localStorage.save(key,myGame.highestScore);
  };

  return self;
})();