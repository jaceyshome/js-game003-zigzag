var BulletsManager = function(){
  var self = this,
      firstRun = true,
      bullets = [];
  self.view = undefined;

  self.init = function(){
    if(!firstRun){
      return;
    }
    firstRun = false;
    self.view = new PIXI.DisplayObjectContainer();
    self.view.position.x = 0;
    self.view.position.y = 0;
  };

  self.update = function(){
    if(firstRun || !bullets.length){
      return;
    }
    var i = 0;
    for(i=0;i<bullets.length;i++){
      if(bullets[i].life){
        bullets[i].update();
      }
    }
  };

  function addBullet(data){
    var newBullet = new Bullet(data);
    self.view.addChild(newBullet.view);
    newBullet.setPosition(data.position);
    activateBullet(newBullet);
    bullets.push(newBullet);
    return newBullet;
  }

  function activateBullet(bullet){
    bullet.life = 1;
    bullet.view.visible = true;
  }

  function checkAvailableBullet(bulletType){
    var i = 0;
    for(i=0;i<bullets.length;i++){
      if(bulletType.sprite == bullets[i].sprite){
        if(!bullets[i].life){
          return bullets[i];
        }
      }
    }
    return false;
  }

  self.getBullets = function(){
    return bullets;
  };

  self.fireBullet = function(data){
    var i = 0,
        availableBullet = checkAvailableBullet(data);
    if(availableBullet){
      availableBullet.setPosition(data.position);
      activateBullet(availableBullet);
    }else{
      availableBullet = addBullet(data);
    }
    myGame.soundManager.playAudio({
      id:availableBullet.uid,
      url:data.sound
    });
  };

  self.init();
  return self;

};