myGame.localStorage = (function(){
  var self = {};

  self.load = function(key){
    if(!window.localStorage){
      return 0;
    }
    if(key){
      return JSON.parse(window.localStorage.getItem(key));
    }
    return false;
  };

  self.save = function(key, data){
    if(!window.localStorage){
      return 0;
    }
    if(key){
      window.localStorage.setItem(key, JSON.stringify(data));
    }
    return false;
  };
  
  return self;
})();