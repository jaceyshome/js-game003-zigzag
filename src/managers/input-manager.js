myGame.inputManager = (function(){
  var firstRun = true;

  //----- public variables
  var bindings = {};
  var actions = {};
  var mouse = {x:0,y:0};
  var move_dir = myGame.move_dir;

  //----- public functions
  function init(){
    if(!firstRun){
      return;
    }
    if(!myGame.renderer.view){
      console.log("Error! Can't not find canvas dom element");
      return;
    }

    document.getElementById(myGame.renderer.view.id).onmousemove = onMouseMove;
    window.onclick = onMouseClick;
    window.onkeydown = onKeyDown;

    //----- private functions ----
    function onMouseMove(event){
      move_dir.x = event.clientX;
      move_dir.y = event.clientY;
    }

    function onMouseClick(){
      console.log("bomb");
    }
  }

  function bind(key, action) {
    bindings[key] = action;
  }

  function onKeyDown(event){
    handlePauseKey(event);
  }

  function handlePauseKey(event){
    if(event.keyCode == 80){
      if(myGame.pausedActivatedScreen){
        myGame.pausedActivatedScreen = false;
      }else{
        myGame.pausedActivatedScreen = true;
      }
    }
  }

  function update(){

  }

  return {
    actions : actions,
    bindings : bindings,
    mouse : mouse,
    bind : bind,
    update: update,
    init : init
  };
})();