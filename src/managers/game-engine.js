myGame.gameEngine = (function(){

  //----- public functions
  var previousCycle = 0,
      animations = [],
      move_dir = myGame.move_dir,
      self = this;

  function renderActivatedScreenAnimations(time, lastTime) {
    var anims = animations.slice(0), // copy list
        n = anims.length,
        animTime,
        anim,
        i;
    for (i=0;i<n;i++) {
      anim = anims[i];
      if (anim.fncs.before) {
        anim.fncs.before(anim.pos);
      }
      anim.lastPos = anim.pos;
      animTime = (lastTime - anim.startTime);
      anim.pos = animTime / anim.runTime;
      console.log("anim.pos", anim.pos);
      anim.pos = Math.max(0, Math.min(1, anim.pos));
    }
    animations = []; // reset animation list
    for (i=0;i<n;i++) {
      anim = anims[i];
      anim.fncs.render(anim.pos, anim.pos - anim.lastPos);
      if (anim.pos == 1) {
        if (anim.fncs.done) {
          anim.fncs.done();
        }
      } else {
        animations.push(anim);
      }
    }
  }

  function update(){
    var inputManager = myGame.inputManager;
    var activeScreen = myGame.activeScreen;
    //    renderActivatedScreenAnimations(time, previousCycle);
    if(inputManager && inputManager.update){
      inputManager.update();
    }
    if(activeScreen && activeScreen.update){
      activeScreen.update();
    }
  }

  function init(){
    myGame.inputManager.init();
  }

  return {
    update : update,
    init: init
  }

})();
