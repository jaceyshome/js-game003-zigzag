myGame.soundManager = (()->
  firstRun = true
  sm = {}
  sm.initialised = false
  sm.autoplay = true
  sm.currentSounds = []
  sm.soundManagerReady

  sm.init = (callback) ->
    return unless firstRun
    firstRun = false
    soundManager.setup({
      url:document.URL + "assets/swfs"
      flashVersion: 9
      debugMode: false
      waitForWindowLoad: true
      useConsole: true
      onready: ->
#        console.log "sound ready"
        sm.soundManagerReady = true
        callback() if callback
        sm.playAudio({url:""})
    })

  sm.stopAll = ->
    soundManager.stopAll()

  sm.stopAudio = (audio)->
    soundManager.stop(audio.audioId)

  sm.playAudio = (audio, callback, onFinishCallback)->
    if sm.soundManagerReady
      player = soundManager.createSound(
        {
          id:audio.id + "_sound"
          url: audio.url
          autoPlay: true
          autoLoad: true
          useWaveformData: false
          useEQData: true
          volume:100
          onfinish:onFinishCallback
          whileplaying:()->
            callback() if callback
        }
      )
      #stop sound before calling play() to  trigger a sound numerous times
      sm.stopAudio(audio)
      player.play()
  return sm
)()

