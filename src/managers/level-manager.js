myGame.levelManager = (function(){
  var self = {},
    firstRun = true,
    level = {};

  function checkHeroScore(){
    if(!myGame.activeScreen.getHeroScore){return;}
    return (myGame.activeScreen.getHeroScore() > level.nextLevel);
  }

  function upgradeLevel(){
    level.currentLevel = level.nextLevel;
    level.nextLevel = level.nextLevel * level.step;
  }

  self.init = function(){
    if(!firstRun){
      return;
    }
    firstRun = false;
    self.reset(myGame.structure.level);
  };

  self.update = function(){
    if(firstRun){
      return;
    }
    if(checkHeroScore()){
      upgradeLevel();
    }
  };

  self.reset = function(){
    level = {
      currentLevel:myGame.structure.level.currentLevel,
      nextLevel: myGame.structure.level.nextLevel,
      step: myGame.structure.level.step
    };
  };

  self.getSpeedOffset = function(){
    return level.currentLevel / 1000;
  };

  return self;
})();