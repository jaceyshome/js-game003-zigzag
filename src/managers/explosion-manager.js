var ExplosionManager = function(customData){
  var self = this,
      firstRun = true,
      explosions = [],
      data = customData || {
        life: 0,
        textureName: "explosion_",
        textureTotal: 24,
        delay:0
      };
  self.view = undefined;

  self.init = function(){
    if(!firstRun){
      return;
    }
    firstRun = false;
//    createExplosion();
  };

  function createExplosion(){
    var explosion = new Explosion(data);
    explosions.push(explosion);
    return explosion;
  }

  self.getExplosion = function (){
    var explosion = checkAvailableExplosion();
    if(!explosion){
      explosion = createExplosion();
    }
    return explosion;
  };

  function checkAvailableExplosion(){
    var i = 0;
    for(i=0;i<explosions.length;i++){
      if(explosions[i].life <= 0){
        return explosions[i];
      }
    }
    return false;
  }

  self.reset = function(){
    explosions = [];
  };

  self.init();
  return self;

};