var CollisionManager = function(data){
  var self = this,
      screenVO = data,
      bulletOffset = -15;

  self.enemyWidthBullets = function(enemy, bullets){
    if(!enemy.life){
      return;
    }
    var i = 0;
    for(i;i<bullets.length;i++){
      if(bullets[i].life && compareWithEnemy(bullets[i].view, enemy)){
        enemy.getDamage(bullets[i].power);
        bullets[i].destroy();
      }
    }
  };

  function compareTwoObjects(from, to){
    if (from.x - from.width / 2 < to.x + to.width / 2 &&
        from.x + from.width / 2 > to.x - to.width / 2 &&
        from.y - from.height / 2 < to.y + to.height / 2  &&
        from.y + from.height / 2 > to.y - to.height / 2 ){
//      testBoundary(from, to);
      return true;
    }
    return false;
  }

  function testBoundary(from, to){
    var fromView = myGame.utilities.showBoundary(from);
    var toView = myGame.utilities.showBoundary(to);
  }

  function compareWithEnemy(target, enemy){
    return (compareWidthEnemyBody(target, enemy) || compareWidthEnemyWing(target, enemy));
  }

  function compareWidthEnemyBody(target, enemy){
    var mask = enemy.getCollisionMasks().body, bodyBoundary, targetBoundary;
    wingBoundary = {
      x: enemy.view.position.x,
      y: enemy.view.position.y + screenVO.enemyContainerOffsetY , //position calculation
      width: mask.width,
      height: mask.height
    };
    targetBoundary = {
      x:target.position.x,
      y:target.position.y,
      width: target.width,
      height: target.height
    };
    return compareTwoObjects(targetBoundary, wingBoundary);
  }

  function compareWidthEnemyWing(target, enemy){
    var mask = enemy.getCollisionMasks().wing, wingBoundary, targetBoundary;
    wingBoundary = {
      x: enemy.view.position.x,
      y: enemy.view.position.y + mask.height / 2  +  mask.y + screenVO.enemyContainerOffsetY , //position calculation
      width: mask.width,
      height: mask.height
    };
    targetBoundary = {
      x:target.position.x,
      y:target.position.y,
      width: target.width,
      height: target.height
    };
    return compareTwoObjects(targetBoundary, wingBoundary);
  }

  self.heroWidthEnemy = function(hero, enemies){
    if(!hero.life){
      return;
    }
    var i = 0, enemy;
    for(i;i<enemies.length;i++){
      if(enemies[i].life && compareWithEnemy(hero.view, enemies[i])){
        hero.destroy();
      }
    }
  };

  return self;

};