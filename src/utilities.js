//public functions

myGame.utilities = (function(){
  var self = {};
  self.pad = function(str, max){
    str = str.toString();
    return str.length < max ? self.pad("0" + str, max) : str;
  };

  self.centralizeObjInScreen = function(width, offset){
    return ((myGame.screenWidth - width) / 2 - (offset || 0));
  };


  self.showBoundary = function(obj){
    var objView = new PIXI.Graphics();
    objView.beginFill("0xFFFFFF");
    objView.drawRect(obj.x - obj.width/2, obj.y - obj.height/2,obj.width,obj.height);
    objView.endFill();
    myGame.stage.addChild(objView);
    return objView;
  };

  return self;
})();